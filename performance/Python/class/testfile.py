"""
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0382 - Create a class constructor or destructor
  S0026 - Utilize standard library modules.
  S0024 - Declare and/or implement container data type.
  S0023 - Declare and implement data types.
  S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0052 - Implement a function that returns a single value.
  S0048 - Implement a function that receives input parameters.
  S0082 - Implement conditional control flow constructs."""
""" Demonstrates: 3.p OOP and Classes"""

import math

""" Create a class Player which represents a character in a game.  The player should have the following attributes:
		name: string representing Paladin's name
		hit_points: float, number of hit points (starts at 100)
		x_pos: int x position on a grid (starts at 0)
		y_pos: int y position on a grid (starts at 0)
		
	Player methods are:
		report_pos: returns a tuple (x_pos, y_pos)
		reduce_health: takes in <float> distance as argument and reduces player hit_points by half the distance.
		move: takes in x_pos and y_pos as arguments, computes the distance by  getting the square root of 
		      (x_pos*x_pos + y_pos*y_pos)
		      calls reduce_health(), and returns hit points.
			- If move is called and it reduces hit_points below 0, return "You are out of hit points!"
		"""
		
''' 

***REVIEWER COMMENTS/APPROVAL****





'''		
class Player(object):
	pass
		


