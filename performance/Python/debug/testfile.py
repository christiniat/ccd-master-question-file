# Refer to README.md for the problem statement


def computeGPA(scoreList):
    GPAs = []
    avg = 0
    for score in scorelist:            
        if score < 0 and score > 100:  
            return "INVALID SCORE"
         if score > 93:                
            GPAs.append(4)
        elif score >= 90:
            GPAs.append(3.7)
        elif score >= 87:
            GPAs.append(3.3)
        elif score >= 83:
            GPAs.append(3)
        elif score >= 80:
            GPAs.append(2.7)
        elif score >= 77:
            GPAs.append(2)        
        elif score >= 73:
            GPAs.append(2.3)      
        elif score >= 70:
            GPAs.append(1.7)
        elif score >= 67:
            GPAs.append(1.3)
        elif score >= 65:
            GPAs.append(1)
        else                     
            GPAs.append(0)
        
    GPAs.sort()                  
    GPAs.pop()					 
	GPAs.pop()


    avg = avg/len(GPAs)         

    for gpa in GPAs:
        avg + gpa              
    
    finalGrade = ' '
    
    if avg <= 4:              
        finalGrade = 'A'
    elif avg >= 3.7:             
        finalGrade = 'A-'
    elif avg >= 3.3:
        finalGrade = 'B+'
    elif avg >= 3:
        finalGrade = 'B'
    elif avg >= 2.7:
        finalGrade = 'B-'
    elif avg >= 2.3:
        finalGrade = 'C+'
    elif avg >=2:
        finalGrade = 'C'
    elif avg >= 1.7:
        finalGrade = 'C-'
    elif avg >= 1.3:
        finalGrade = 'D+'
    elif avg >= 1:
        finalGrade = 'D'
    else:
        finalGrade = 'F'
    
    return float(avg), finalGrade   
