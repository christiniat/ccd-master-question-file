# KSAT List
This question is intended to evaluate the following topics:
- S0054 - Create and destroy a Weighted Graph.
- S0063 - Find item in a Weighted Graph.
- S0064 - Add and remove nodes from a a Weighted Graph.
- S0379 - Demonstrate ability to write object-oriented programs
- S0048 - Implement a function that receives input parameters.


# Task
Create a class, WeightedGraph that implements a weighted, directional graph. 

The class must support the following functions:
- A constructor with no parameters

- add_node(node_id, value) - Given an index value for the node, and a value to store within the node, add that node to 
  the graph. 

- add_edge(source, dest, cost) - Create an edge FROM the source node TO the dest in the graph, with a weight of cost. 
  - Nodes must support multiple edges. Edges must be one-way.

- delete_node(node_id) - Remove a node with the specified index from the graph, as well as all edges connected to that 
  node. 
  - Must gracefully handle the case where a node does not exist.

- get_nodes() - Return a list of nodes in the graph with the following tuple format for each element: (index, value)
    
- get_edges() - Return a list of all edges in the graph with the following tuple format for each element: 
  (source_node_index, dest_node_index, cost)

- traverse(start, end) - Implement greedy DFS to traverse the graph from the start node to the end node, choosing the 
  shortest possible edge at each step.  Return the path taken as a list in order of traversal.
  - Only nodes along the path should be included. 
  - Assume no ties in weight. 
  - Return None if there is no valid path or either node does not exist.

The class may be implemented with any method as long as the above functions have their criteria met. 

YOU MAY NOT USE ANOTHER MODULE TO IMPLEMENT THE GRAPH FOR YOU
