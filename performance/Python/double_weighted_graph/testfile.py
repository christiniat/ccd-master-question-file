# Refer to README.md for the problem statement


class WeightedGraph:

    def add_node(self, node_id, value):
        pass

    def add_edge(self, node_id, dest_id, cost):
        pass

    def delete_node(self, node_id):
        pass

    def get_nodes(self):
        pass

    def get_edges(self):
        pass

    def traverse(self, start, end):
        pass
