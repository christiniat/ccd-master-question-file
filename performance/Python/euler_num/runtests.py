import unittest, xmlrunner
from testfile import *


class CheckRideTestOne(unittest.TestCase):
    def test_compute_euler(self):
        self.assertEqual(2.71828, compute_euler())


if __name__ == '__main__':
    with open('unittest.xml', 'w') as output:
        unittest.main(
        testRunner=xmlrunner.XMLTestRunner(output=output), 
        failfast=False, 
        buffer=False, 
        catchbreak=False
        )






