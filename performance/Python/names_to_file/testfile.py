"""
This question is intended to evaluate the following topics:
  A0047 - Implement file management operations.
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0024 - Declare and/or implement container data type.
  S0023 - Declare and implement data types.
  S0031 - Utilize logical operators to formulate boolean expressions.
  S0037 - Open and close an existing file.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0038 - Read, parse, write (append, insert, modify) file data.
  S0039 - Create and delete a file.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0048 - Implement a function that receives input parameters.
  S0079 - Validate expected input.
  S0081 - Implement a looping construct.
  S0082 - Implement conditional control flow constructs."""
'''
Write the function create_file that receives a list of lists. Each list
within the list has two strings representing a first and last name. 
Each last name will be unique, you will not have to worry about two of the same last names.

The function will process the list and write each name sorted in 
alphabetical order by last name to a file called names.txt in the format
of Last, First with each name on a separate line

Example:  Hancock, John
          Washington, George

When writing to file, ensure both first and last name are capitalized.


If the passed list is empty, return the string "EMPTY_LIST"

If any of the list items in the list have more or less than two items,
or have empty strings, return the string "INVALID_NAME"

If the list is processed successfully and the file is written, return the string "SUCCESS"
'''

def create_file(names):
    return ''
