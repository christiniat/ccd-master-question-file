import unittest, xmlrunner
from testfile import *


class CheckRideTestOne(unittest.TestCase):
    def test_get_Perfect_Numbers(self):
        self.assertEqual({6, 28, 496}, getPerfectNumbers())

    def test_perfect(self):
        self.assertTrue(perfect(8128))

    def test_not_perfect(self):
        self.assertFalse(perfect(8127))


if __name__ == '__main__':
    unittest.main()





