"""
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0026 - Utilize standard library modules.
  S0023 - Declare and implement data types.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0052 - Implement a function that returns a single value.
  S0048 - Implement a function that receives input parameters.
  S0082 - Implement conditional control flow constructs."""
"""
Write a function get_number_from_string that searches a string and matches a valid number.
A number can have any number of digits, but it can have only digits and a decimal point.
The decimal point is optional,but if it appears in the number, there must be only one,
and it must have digits on its left and its right.
There must be whitespace(s) on both sides of a valid number.
Negative numbers are preceded by a minus sign.

if a number is found, return a floating point value for the number

Return the first valid number found

if a number is not found, return the string "No number"

"""


def get_number_from_string(testString):
    return 0



