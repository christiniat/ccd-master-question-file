"""
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0024 - Declare and/or implement container data type.
  S0023 - Declare and implement data types.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0052 - Implement a function that returns a single value.
  S0081 - Implement a looping construct.
  S0082 - Implement conditional control flow constructs."""
"""
Write a function duplicate_Values(aDictionary) that takes a dictionary of 20 values in the range 1- 99,
and determines whether there are any duplicate values in the dictionary.

The function duplicate_Values(aDictionary) returns 1 if there are duplicate values and 0 otherwise

"""

def duplicateValues(aDictionary):
    """
     There are many ways to determine if there are duplicate values.
     1- One can create an array of 100 bits (initialized to '0') and set the bit to '1' at the index of the number
        in the dictionary. If the bit at that index is already '1' that means we have duplicate values.

     2- Another simple way is to sort the values of the dictionary then look if there are duplicate values

    """
    return 0

