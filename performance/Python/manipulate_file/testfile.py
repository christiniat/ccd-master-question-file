"""
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0023 - Declare and implement data types.
  S0031 - Utilize logical operators to formulate boolean expressions.
  S0037 - Open and close an existing file.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0038 - Read, parse, write (append, insert, modify) file data.
  S0033 - Utilize assignment operators to update a variable.
  S0041 - Determine location of content within a file.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0080 - Demonstrate the skill to implement exception handling.
  S0079 - Validate expected input.
  S0081 - Implement a looping construct.
  S0082 - Implement conditional control flow constructs."""
'''
Write the function manipulateFile that receives four parameters:
  fileName - the name of a file that you will read/write/append to/from
  findWord - a word to search for in the file
  insertWord - a word that will be inserted in the file
  appendWord - a word that will be appended to the end of the file

The function should read the file in fileName and determine if the string in findWord exists in the file.  If it does
exist, insert a space and the string in insertWord immediately after the findWord.

For example, the file may contain the following text:

The quick brown fox jumped 

If findWord contained "quick" and the insertWord contained "sly", once processed the line in the file would have:

The quick sly brown fox jumped

The string in appendWord will simply be appended to the end of the file preceded by a space. For example, the end of the
file may contain:

over the lazy dog's back

if appendWord contains "fence", once processed the line in the file would have:

over the lazy dog's back fence

if the file in fileName does not exist, the function should return the string: "FILE_ERROR"
if the string in findWord does not exist in the file, the function should return the string: "WORD_NOT_FOUND"
and make no changes to the file.
otherwise, the function should return "SUCCESS"

NOTE: during your development, should the data files under test get corrupted, pristine copies are located in the
'original files' folder.  Simply make a copy and bring over into your working directory.

'''


def manipulateFile(fileName, findWord, insertWord, appendWord):

	return "SUCCESS"
