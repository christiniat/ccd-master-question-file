# KSAT List
- S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0117 - Utilize labels for control flow.
- S0125 - Utilize general purpose instructions.
- S0118 - Implement conditional control flow.
- S0136 - Utilize x86 calling conventions
- S0134 - Utilize registers and sub-registers
- S0177 - Write assembly code that accomplishes the same functionality as given in a high level language (C or Python) code snippet.

# Task
Implement functionality that returns the number of times a given character occurs in a string.
