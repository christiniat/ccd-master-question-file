#include <Windows.h>
#include <stdio.h>
#include "test_defs.h"

extern void addvalues(int*, int*, int*);

TEST(FifthTask, addvalues)
{
	size_t oldEsp = 0;
	size_t newEsp = 0;

	// input
	int x[4];
	int y[4];

	// output
	int z[4] = { 0 };

	// validation
	int t[4] = { 0 };

	
	srand(GetTickCount());
	for (int i = 0; i < 4; ++i)
		x[i] = rand();

	for (int i = 0; i < 4; ++i)
		y[i] = rand();

	__asm { mov oldEsp, esp }
	addvalues(x, y, z);
	__asm { mov newEsp, esp }

	if (oldEsp != newEsp)
		FAIL_WITH_CODE(AssertFailStackUnbalanced);

	for (int i = 0; i < 4; ++i)
		t[i] = x[i] + y[i];

	ASSERT_EQUAL(0, memcmp(z, t, sizeof(int) * 4));

}
