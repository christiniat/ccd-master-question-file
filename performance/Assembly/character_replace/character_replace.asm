bits 32

section .text

global _character_replace


; Refer to README.md for the instructions
;
; char* __cdecl character_replace(char* oldString, char* newString, char oldChar, char replaceChar)
_character_replace:
	push ebp
	mov ebp, esp

	; Stack addresses of interest
	; ebp + 0x08 original (input) string
	; ebp + 0x0C finished (output) string
	; ebp + 0x10 original character
	; ebp + 0x14 replacement character

	;; Save ebx
	push ebx

	; Your code
	
	pop ebx

	mov eax, [ebp+0x0c]

	mov esp, ebp
	pop ebp

	ret
