#include <Windows.h>
#include <stdio.h>
#include <string.h>
#include "test_defs.h"
#define MAX 25

extern void power_of_2(int);


// Tests assembly that calculates an exponential value of 2. (2 ^ n = x)
TEST(PowerOf2Task, power_of_2)
{
	size_t oldEsp = 0;
	size_t newEsp = 0;
	size_t retreived_ebp = 0;

	// input
	int x = 5;

	// output
	CONST int z = 32;

	__asm { mov oldEsp, esp };

	/* Call power_of_2 assembly and then test the return value */
	power_of_2(x);
	int copy_eax = 0;
	__asm { mov copy_eax, eax };
	ASSERT_EQUAL(z, copy_eax);

	__asm { mov newEsp, esp }

	if (oldEsp != newEsp)
		FAIL_WITH_CODE(AssertFailStackUnbalanced);

}

