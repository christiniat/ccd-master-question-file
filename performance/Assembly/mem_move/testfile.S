bits 32

section .text

global _movememory

; Refer to README.md for the instructions
;
; void __cdecl movememory(void* dest, void* src, size_t length)
_movememory:
	push ebp
	mov ebp, esp

	push edi
	push esi
	push ebx

	mov edi, [ebp+0x8]	;dest
	mov esi, [ebp+0xc]	;src
	mov ecx, [ebp+0x10]	;length

	;// code begin //



	;//  code end  //

	pop edi
	pop esi
	pop ebx

	mov esp, ebp
	pop ebp

	ret
