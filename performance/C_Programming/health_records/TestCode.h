#pragma once

#define LINE_LENGTH 80
#define NUMBER_OF_PERSONS 5
#define NAME_LENGTH 15 // The examinee should define this and the two structs

#ifdef __cplusplus
extern "C" {
#endif
    // Task One
    


    void readData(HealthProfile *healthDatabase);
    int calculateAgeInYears(HealthProfile hp);
    double calculateBMI(HealthProfile hp);
    int calculateMaxHeartRate(int age);
    double calculateMaxTargetHeartRate(int maxHR);
    double calculateMinTargetHeartRate(int maxHR);
    HealthProfile *processHealthProfiles();

#ifdef __cplusplus
}
#endif