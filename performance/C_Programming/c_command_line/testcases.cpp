#include <stdlib.h>
#include <gtest/gtest.h>
#include "testcases.h"
#include "TestCode.h"
#include <stdio.h>
#include <stddef.h>

void checkFile(const char *fname, const char *words[], int wordslen)
{
    FILE *fp;
    int buflen = 128;
    char buf[buflen];
    int i = 0;
    int cmp;
    fp = fopen(fname, "r");
    ASSERT_FALSE(fname == NULL);
    ASSERT_FALSE(words == NULL);
    ASSERT_FALSE(fp == NULL);
    while(fgets(buf, buflen, fp))
    {
        buf[strcspn(buf, "\n")] = '\0';
        cmp = strcmp(words[i], buf);
        if(cmp)
        {
            printf("words[i]: %s\n buf: %s\n", words[i], buf);
        }
        ASSERT_EQ(0, cmp);
        i++;
    }
}

TEST(FileDumpTests, validInfo)
{
	const char *fname = "fileDumpTest.txt";
    const char *words[] = {"apple", "bagel", "cranberry", "donut"};

    remove(fname);
    int numWords = 4;
    fileDump(fname, words, numWords);

    checkFile(fname, words, numWords);
}

TEST(FileDumpTests, NoArgs)
{
	const char *fname = "noArgs.txt";
    const char *words[] = {};
    int numWords = 0;

    remove(fname);
    int status = fileDump(fname, words, numWords);

    ASSERT_TRUE(status);
    checkFile(fname, words, numWords);
}

TEST(FileDumpTests, nullFname)
{
	const char *fname = NULL;
    const char *words[] = {};
    int numWords = 0;
    int status = fileDump(fname, words, numWords);

    ASSERT_FALSE(status);
}

TEST(FileDumpTests, nullWords)
{
	const char *fname = "nullArgs.txt";
    const char **words = NULL;
    int numWords = 0;

    remove(fname);
    int status = fileDump(fname, words, numWords);

    ASSERT_FALSE(status);
    ASSERT_TRUE(fopen(fname, "r") == NULL);
}

TEST(CommandlineTests, normalUse)
{
    const char *words[] = {"apple", "bagel", "cranberry", "donut"};
	const char *command = "./TestCode -f testresults.txt apple bagel cranberry donut";

    remove("testresults.txt");
    int status = system(command);
    checkFile("testresults.txt", words, 4);
}

TEST(CommandlineTests, longOption)
{
    const char *words[] = {"apple", "bagel", "cranberry", "donut"};
	const char *command = "./TestCode --filename testresults.txt apple bagel cranberry donut";

    remove("testresults.txt");
    int status = system(command);
    checkFile("testresults.txt", words, 4);
}

TEST(CommandlineTests, defaultFile)
{
    const char *words[] = {"apple", "bagel", "cranberry", "donut"};
    const char *command = "./TestCode apple bagel cranberry donut";

    remove("text.txt");
    int status = system(command);
    checkFile("text.txt", words, 4);
}

TEST(CommandlineTests, fileNameIsParam)
{
    const char *words[] = {"bagel", "cranberry", "donut"};
    const char *command = "./TestCode -f apple bagel cranberry donut";

    remove("apple");
    int status = system(command);
    checkFile("apple", words, 3);
}

TEST(CommandlineTests, fileNameIsParamLong)
{
    const char *words[] = {"bagel", "cranberry", "donut"};
    const char *command = "./TestCode --filename apple bagel cranberry donut";

    remove("apple");
    int status = system(command);
    checkFile("apple", words, 3);
}

TEST(CommandlineTests, paramsWithNoFileName)
{
    const char *words[] = {"apple", "bagel", "cranberry", "donut"};
    const char *command = "./TestCode apple bagel cranberry donut -f";

    remove("text.txt");
    int status = system(command);
    checkFile("text.txt", words, 4);
}

TEST(CommandlineTests, NoArgs)
{
    const char *words[] = {"", "", "", ""};
    const char *command = "./TestCode";

    remove("text.txt");
    int status = system(command);
    checkFile("text.txt", words, 4);
}

TEST(CommandlineTests,fileNoArgs)
{
    const char *words[] = {"", "", "", ""};
    const char *command = "./TestCode -f testNoArg.txt";

    remove("testNoArg.txt");
    int status = system(command);
    checkFile("testNoArg.txt", words, 4);
}

int doTest()
{
    int status = 0;
    char envVar[] = "CCOMMANDLINETEST= Running";
    const char *envVarName = "CCOMMANDLINETEST";
    ::testing::InitGoogleTest();

    // Set this so we only call the tests once
    putenv(envVar);

    status = RUN_ALL_TESTS();

    // Ensure the custom variable is removed before exiting
    unsetenv(envVarName);

    return status;
}
