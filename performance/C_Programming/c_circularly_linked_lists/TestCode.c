/*
This question is intended to evaluate the following topics:
  A0019 - Integrate functionality between multiple software components.
  A0018 - Analyze a problem to formulate a software solution.
  S0034 - Declare and implement appropriate data types for program requirements.
  S0035 - Declare and/or implement of arrays and multi-dimensional arrays.
  S0032 - Utilize relational operators to formulate boolean expressions.
  S0033 - Utilize assignment operators to update a variable.
  S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
  S0069 - Find an item in a Circularly Linked List.
  S0070 - Add and remove nodes from a Circularly Linked List.
  S0052 - Implement a function that returns a single value.
  S0057 - Create and destroy a Circularly Linked List.
  S0053 - Implement a function that returns a memory reference.
  S0048 - Implement a function that receives input parameters.
  S0079 - Validate expected input.
  S0090 - Allocate memory on the heap (malloc).
  S0097 - Create and use pointers.
  S0091 - Unallocating memory from the heap (free).
  S0081 - Implement a looping construct.
  S0108 - Utilize post and pre increment/decrement operators.
  S0082 - Implement conditional control flow constructs.
  S0156 - Utilize a struct composite data type.
  S0160 - Utilize the standard library.*/
/*

Task #1

Write the function buildCList that receives a pointer to an array of integers (nums)
and the size of the array (size).

The function should create a circular linked list using 
the numNode struct defined below.

The function should iterate the "nums" array and insert the numbers into nodes. 
Each new node will be inserted at the head of the circularly linked list.

if a number in "nums" is already in the circularly linked list, do not insert it into the
circularly linked list and continue iterating the "nums" list

The function should return a reference to the head node of the link list once processed.

if the size passed is 0, the function should return NULL.

Task #2

Write the function emptyList that receives a pointer (head) to the head node of the
circularly linked list, and removes each node and frees all the memory. The function should return the number of nodes removed.

if "head" is NULL, the function should return zero (0).

*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "TestCode.h"

struct numNode * buildCList(int *nums, int size)
{
    
    return NULL;
}

int emptyList(struct numNode *head)
{
	
	return 0;
}
