# KSAT List
- A0018 - Analyze a problem to formulate a software solution.
- S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
- S0034 - Declare and implement appropriate data types for program requirements.
- S0031 - Utilize logical operators to formulate boolean expressions.
- S0032 - Utilize relational operators to formulate boolean expressions.
- S0033 - Utilize assignment operators to update a variable.
- S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0052 - Implement a function that returns a single value.
- S0048 - Implement a function that receives input parameters.
- S0079 - Validate expected input.
- S0081 - Implement a looping construct.
- S0108 - Utilize post and pre increment/decrement operators.
- S0082 - Implement conditional control flow constructs.

# Task
Write a function `bin_hex_StrToInt32` that receives a string containing either a binary or hex number. 
The function must return the string’s integer value. 

Treat string occurences containing ONLY binary characters as binary, all other cases should be treated as hex.

If the input parameter is empty or invalid value, the function should return ERROR_INVALID_PARAMETER

Note: Do not call built-in library functions that accomplish these tasks automatically.
