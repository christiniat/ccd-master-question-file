#include <gmock/gmock.h>
#include "TestCode.h"

// CProgrammingTests.cpp : Defines the entry point for the console application.
#define _CRT_SECURE_NO_WARNINGS 1
#define _SILENCE_TR1_NAMESPACE_DEPRECATION_WARNING 1


TEST(TestCase0, functionChoice0)
{
    int i = 0;
    int refSet[] = {1,2,4,4,5,8};
    int dataSet[] = {5,4,8,4,1,2};
    sort(dataSet,6, ascending);
    for(i; i < 6; i++)
    {
        ASSERT_EQ(dataSet[i], refSet[i]);
    }

}

TEST(TestCase1, functionChoice1)
{
    int i = 0;
    int refSet[] = {8,5,4,4,2,1};
    int dataSet[] = {5,4,8,4,1,2};
    sort(dataSet, 6, descending);
    for(i; i < 6; i++)
    {
        ASSERT_EQ(dataSet[i], refSet[i]);
    }

}


