# KSATs
This question is intended to evaluate the following topics:
- A0004: Write pseudocode to solve a programming problem
- A0018: Analyze a problem to formulate a software solution
- S0171: Write Pseudocode for a sequential process
- S0172: Write Pseudocode with repeating steps to the process

# Task
You are working a new requirement that is still in the early planning stage. This project involves a system that allows
users to create and read personal journal entries. You need to create a pseudocode implementation of the requirements 
in order to assist with design planning. The getNumJournal and getJournals components of this requirement were 
previously completed but may still need integration into the remaining requirements. The customer wants separate read
and update journal options; however, your team will only be able to support the read option at this time. Your design 
needs to be modular and extendable so the update feature can be easily added later.

# Journal System Requirements
This requirement provides a command-line based utility allowing users to make a choice from several menu options in 
order to interact with the journal system.
- The utility shall work with any Linux based OS; functionality with other OSs is not desired.
- The utility shall present the user with a Main Menu.
- The Main Menu shall present options to:
  - Create a new journal
  - Read an existing journal
  - Exit the utility
- The utility shall allow the user to enter another menu option if invalid input was given; this should be the case for 
  all menus.
- The utility shall establish a journal storage location for all journals.
- The module representing the `Create a new journal` menu option:
  - shall allow a user to enter a file name from the command prompt 
  - shall create a new journal with the specified name in the journal storage location
- The `Read an existing journal` menu option shall present the user with a journal menu.
- The journal menu:
  - shall display the currently available journals as selectable options
  - shall allow a user to return to the main menu
  - shall allow a user to read a valid journal
- The module representing the read journal option shall allow a user to view a journal option from the command 
prompt so the user can move up and down in the journal

The following requirements already have the necessary pseudocode but still need integration into the Journal Menu.
- The `getNumJournals` module 

  - takes in a string representing the path to the system journal storage location
  - returns an integer representing the number of journals in the system

- The `getJournals` module 

  - takes in a string representing the path to the system journal storage location
  - takes in a buffer.
    - The buffer is filled with the paths to each journal in the system
    - Each path is restricted to 256 max length
  - takes in an integer representing the max number of strings in the buffer

# Hierarchy Chart
The chart below explains the code hierarchy for the Journal System.

```mermaid
flowchart TD
    MainMenu ---> |Invalid Entry| MainMenu;
    MainMenu ---> CreateJournal;
    MainMenu ---> JournalMenu;
    MainMenu --> |Finished| Exit;
    JournalMenu --> |Finished| MainMenu;
    JournalMenu --> |Invalid Entry| JournalMenu;
    JournalMenu <--> GetNumJournals;
    JournalMenu <--> GetJournals;
    JournalMenu ---> ReadJournal;
```
