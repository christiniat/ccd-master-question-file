# Table of Contents
- [Compile Instructions](#compile-instructions)
  - [1. Build/Compile/Run C_Programming](#1-buildcompilerun-c_programming)
    - [1.1 Use the VSCode Debugger (Easiest)](#11-use-the-vscode-debugger-easiest)
    - [1.2 Use CMake in a VSCode terminal](#12-use-cmake-in-a-vscode-terminal)
  - [2. Run Python and Networking](#2-run-python-and-networking)
    - [2.1 Use the VSCode Debugger (Easiest)](#21-use-the-vscode-debugger-easiest)
    - [2.2 Use Python3 in VSCode terminal](#22-use-python3-in-vscode-terminal)
  - [3. Appendix](#3-appendix)
    - [3.1 C Programming Build Output](#31-c-programming-build-output)
    - [3.2 C Programming Compile Output](#32-c-programming-compile-output)
    - [3.3 C Programming Run Output](#33-c-programming-run-output)
    - [3.4 Python and Networking Run Output](#34-python-and-networking-run-output)

# Compile Instructions
**While in VSCode, render this file with `ctrl+shift+V`**

\[ [TOC](#table-of-contents) \]

## 1. Build/Compile/Run C_Programming

### 1.1 Use the VSCode Debugger (Easiest)
1. Place breakpoints if needed to step through your process. Click to the left of the line numbers in `TestCode.c`
   - Keep tab focus on `TestCode.c`
2. Press `ctrl+shift+D` or click the `Run` icon on the left side of VSCode to open the debug runner
3. Click the dropdown at the top and select `Test C`
4. Press the play button
5. VSCode will automatically open a terminal and proceed to build, compile, and run your program. Example output shown 
   in [appendix 3.1](#31-c-programming-build-output), [3.2](#32-c-programming-compile-output), and 
   [3.3](#33-c-programming-run-output)
   - Make sure you're viewing the `Terminal` tab and not the `Debug Console`

\[ [TOC](#table-of-contents) \]

### 1.2 Use CMake in a VSCode terminal
1. Open a VSCode terminal with `ctrl+shift+~`
2. Change directory (`cd`) into the question folder
   - `cd C_Programming/buy_groceries`
3. Copy/Paste the following command and run it
```sh
cmake -B $PWD/build -S $PWD && cmake --build $PWD/build --target TestCode -- -j 6 && ./build/TestCode
```
4. This will proceed to build, compile, and run your program. Example output shown in 
   [appendix 3.1](#31-c-programming-build-output), [3.2](#32-c-programming-compile-output), and 
   [3.3](#33-c-programming-run-output)
   - Make sure you're viewing the `Terminal` tab and not the `Debug Console`

\[ [TOC](#table-of-contents) \]


## 2. Run Python and Networking

### 2.1 Use the VSCode Debugger (Easiest)
1. Place breakpoints if needed to step through your process. Click to the left of the line numbers in `testfile.py` 
   (or `client.py` for networking)
   - Keep tab focus on `testfile.py` (or `client.py` for networking)
2. Press `ctrl+shift+D` or click the `Run` icon on the left side of VSCode to open the debug runner
3. Click the dropdown at the top and select `Test Python` (or `Test Networking` if running the Networking python file)
4. Press the play button
5. VSCode will automatically open a terminal and proceed to run your program shown in 
   [appendix 3.4](#34-python-and-networking-run-output)
   - Make sure you're viewing the `Terminal` tab and not the `Debug Console`

\[ [TOC](#table-of-contents) \]

### 2.2 Use Python3 in VSCode terminal
1. Open a VSCode terminal with `ctrl+shift+~`
2. Change directory (`cd`) into the question folder
   - `cd Python/count_time`
3. Copy/Paste the following command and run it (This applies to both Python and Networking questions)
```sh
python3 runtests.py
```
4. This will proceed to run your program. Example output shown in [appendix 3.4](#34-python-and-networking-run-output)
   - Make sure you're viewing the `Terminal` tab and not the `Debug Console`

\[ [TOC](#table-of-contents) \]


## 3. Appendix

### 3.1 C Programming Build Output
```txt
-- The C compiler identification is GNU 9.3.0
-- The CXX compiler identification is GNU 9.3.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/bin/cc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/c++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Found PythonInterp: /usr/bin/python3.8 (found version "3.8.5") 
-- Looking for pthread.h
-- Looking for pthread.h - found
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD - Failed
-- Looking for pthread_create in pthreads
-- Looking for pthread_create in pthreads - not found
-- Looking for pthread_create in pthread
-- Looking for pthread_create in pthread - found
-- Found Threads: TRUE  
-- Configuring done
-- Generating done
-- Build files have been written to: C_Programming/buy_groceries/build
```

\[ [TOC](#table-of-contents) \]

### 3.2 C Programming Compile Output
```txt
Scanning dependencies of target gtest
[ 11%] Building CXX object googletest/googletest/CMakeFiles/gtest.dir/src/gtest-all.cc.o
[ 22%] Linking CXX static library ../../lib/libgtestd.a
[ 22%] Built target gtest
Scanning dependencies of target gmock
[ 33%] Building CXX object googletest/googlemock/CMakeFiles/gmock.dir/src/gmock-all.cc.o
[ 44%] Linking CXX static library ../../lib/libgmockd.a
[ 44%] Built target gmock
Scanning dependencies of target gmock_main
[ 55%] Building CXX object googletest/googlemock/CMakeFiles/gmock_main.dir/src/gmock_main.cc.o
[ 66%] Linking CXX static library ../../lib/libgmock_maind.a
[ 66%] Built target gmock_main
Scanning dependencies of target TestCode
[ 88%] Building CXX object CMakeFiles/TestCode.dir/testcases.cpp.o
[ 88%] Building C object CMakeFiles/TestCode.dir/TestCode.c.o
[100%] Linking CXX executable TestCode
[100%] Built target TestCode
```

\[ [TOC](#table-of-contents) \]

### 3.3 C Programming Run Output
Will output relative to the question's unit tests.
```txt
Running main() from gmock_main.cc
[==========] Running 2 tests from 2 test suites.
[----------] Global test environment set-up.
[----------] 1 test from TestCase1
[ RUN      ] TestCase1.buyGroceriesTest_cornerCases
[       OK ] TestCase1.buyGroceriesTest_cornerCases (0 ms)
[----------] 1 test from TestCase1 (0 ms total)

[----------] 1 test from TestCase2
[ RUN      ] TestCase2.buyGroceriesTest_normalCases
[       OK ] TestCase2.buyGroceriesTest_normalCases (0 ms)
[----------] 1 test from TestCase2 (0 ms total)

[----------] Global test environment tear-down
[==========] 2 tests from 2 test suites ran. (0 ms total)
[  PASSED  ] 2 tests.
```

\[ [TOC](#table-of-contents) \]

### 3.4 Python and Networking Run Output
```txt
Running tests...
----------------------------------------------------------------------
..
----------------------------------------------------------------------
Ran 2 tests in 0.001s
OK
```

\[ [TOC](#table-of-contents) \]
