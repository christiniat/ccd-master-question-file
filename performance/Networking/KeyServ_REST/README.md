# KSAT List
This question is intended to evaluate the following topics:
- A0019 - Integrate functionality between multiple software components.
- A0018 - Analyze a problem to formulate a software solution.
- A0626 - String-based Protocol
- S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
- S0096 - Utilize the Socket library.

# Task
The objective of this task is to utilize a relatively simple REST API provided by a webserver running on port 5000 of 
the grader system to obtain a key.

You will need to create two functions, each that handles a request.
- `do_key()`: /user/\<user_id\>/ url accepts a fictional HTTP "KEY" method, which will return a key value. This 
  function will return the key.
- `do_put()`: you will send the key value you received via an HTTP "PUT" method, /key/\<user_id\>/\<key_val\>/. This 
  function will return the response.

# Network Communication Map
Use this example for understanding the sequence that the Client/Server will communicate.

```text
CLIENT ----------------------> SERVER

The CLIENT sends a fictional HTTP "KEY" method using the provided URL. ex: KEY /user/<user_id>/ HTTP/1.1 

CLIENT <---------------------- SERVER

The SERVER sends back one of two messages. 
1. If successful, you will receive a key.
   ex: b'a9b5c7e94bc3e95b4b42a69d13bfa2cd68d1ea8cd30356587c5c1f6b273fec197b1d249d3862ad7c14594b1b2eaeaac3b6635fb5eae2a0c318bd6aa7547371b6'
2. If unsuccessful, you will receive either a 400, 404 or 405 and a message describing the error.

CLIENT ----------------------> SERVER

The CLIENT sends a HTTP "PUT" method using the provided URL. ex: PUT /key/<user_id>/<key_val>/ HTTP/1.1 

CLIENT <---------------------- SERVER

The SERVER sends back one of two messages. 
1. If successful, you will receive a message indicating your success.
2. If unsuccessful, you will receive a message indicating the problem.
```
