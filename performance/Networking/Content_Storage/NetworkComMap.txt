CONTENT MANAGER (You) ---------> REMOTE SERVER

Informs the REMOTE SERVER of the port it should use for communication

CONTENT MANAGER <--------- REMOTE SERVER

Sends the CONTENT MANAGER data to be saved to a specified file name

CONTENT MANAGER <>

After receiving the data, the connection is closed
The data is written to the specified file
The SERVER continues listening for another connection